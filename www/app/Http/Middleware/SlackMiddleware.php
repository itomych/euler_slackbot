<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlackMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if (! $request->input('token') === env('SLACK_VERIFICATION_TOKEN')) {
            return response()->json(['error' => 'Unauthorized.'], Response::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}

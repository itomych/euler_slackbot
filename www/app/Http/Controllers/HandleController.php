<?php

namespace App\Http\Controllers;

use App\Exceptions\InconsistentMessageException;
use App\Exceptions\SlackBadRequest;
use App\Services\SlackEulerService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HandleController extends Controller
{
    /**
     * @var SlackEulerService
     */
    private $slackEulerService;

    /**
     * HandleController constructor.
     * @param SlackEulerService $slackEulerService
     */
    public function __construct(SlackEulerService $slackEulerService)
    {
        $this->slackEulerService = $slackEulerService;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws InconsistentMessageException
     * @throws SlackBadRequest
     */
    public function handle(Request $request)
    {
        if ($request->challenge) {
            return $request->challenge;
        }

        $this->slackEulerService->handle([
            'channel' => $request->input('event.channel'),
            'text' => $request->input('event.text')
        ]);

        return response()->json('', Response::HTTP_OK);
    }
}

<?php

namespace App\Services;

use App\Exceptions\InconsistentMessageException;
use App\Exceptions\SlackBadRequest;
use GuzzleHttp\Client;

class SlackService
{
    private const POST_MESSAGE_METHOD = 'chat.postMessage';

    private $client;

    private $token;

    /**
     * SlackService constructor.
     */
    public function __construct()
    {
        $this->token = env('SLACK_BOT_ACCESS_TOKEN');
        $this->client = new Client([
            'base_uri' => env('SLACK_API_BASE_URL')
        ]);
    }

    /**
     * @param array|string $content
     * @return void
     * @throws SlackBadRequest
     * @throws InconsistentMessageException
     */
    public function chatMessage($content): void
    {
        $formData = $this->buildFormData([
            'channel' => $content['channel'],
            'content' => $this->composeMessage($content['message'])
        ]);

        $response = $this->client->post(self::POST_MESSAGE_METHOD, [
            'form_params' => $formData
        ]);

        $body = json_decode($response->getBody());

        if (!$body->ok) {
            throw new SlackBadRequest($body->error);
        }
    }

    /**
     * @param array|null $message
     * @return array
     */
    private function buildFormData($message = null): array
    {
        return [
            'token' => $this->token,
            'channel' => $message['channel'],
            'text' => ' ',
            'blocks' => json_encode($message['content'])
        ];
    }

    /**
     * @param array $content
     * @return array
     * @throws InconsistentMessageException
     */
    private function composeMessage($content): array
    {
        if (!$this->validateMessage($content)) {
            throw new InconsistentMessageException('Some message data is missing.');
        }
        return [
          [
              'type' => 'section',
              'text' => [
                  'type' => 'mrkdwn',
                  'text' => $content['title']
              ],
          ],
          [
              'type' => 'section',
              'text' => [
                  'type' => 'mrkdwn',
                  'text' => $content['text']
              ],
          ]
        ];
    }

    /**
     * @param $content
     * @return bool
     */
    private function validateMessage($content): bool
    {
        return !empty($content['title']) && !empty($content['text']);
    }
}

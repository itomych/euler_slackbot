<?php

namespace App\Services;

use App\Exceptions\InconsistentMessageException;
use App\Exceptions\SlackBadRequest;

class SlackEulerService
{
    /**
     * @var SlackService
     */
    private $slackService;

    /**
     * SlackEulerService constructor.
     * @param SlackService $slackService
     */
    public function __construct(SlackService $slackService)
    {
        $this->slackService = $slackService;
    }

    /**
     * @param array $message
     * @throws InconsistentMessageException
     * @throws SlackBadRequest
     */
    public function handle(array $message): void
    {
        $text = $this->cleanUpMessage($message['text']);

        if ($number = $this->getNumber($text)) {
            if (!$this->validateNumber((int)$number)) {
                $this->slackService->chatMessage([
                    'channel' => $message['channel'],
                    'message' => [
                        'title' => 'Hi `username`',
                        'text' => 'If you want to know sum of multiples 3 and 5 until some number you can mention me with a number which is greater then 0 and less then 10000: [at]Euler 199'
                    ]
                ]);
            } else {

                $sum = $this->calculate($number);

                $this->slackService->chatMessage([
                    'channel' => $message['channel'],
                    'message' => [
                        'title' => 'Are you ready?',
                        'text' => 'Sum of multiples 3 and 5 until ' . $number . ' is ' . $sum
                    ]
                ]);
            }

        } else {

            $this->slackService->chatMessage([
                'channel' => $message['channel'],
                'message' => [
                    'title' => 'Hi `username`',
                    'text' => 'If you want to know sum of multiples 3 and 5 until some number you can mention me with a number which is greater then 0 and less then 10000: [at]Euler 199'
                ]
            ]);
        }
    }

    /**
     * @param $number
     * @return int
     */
    private function calculate($number): int
    {
        $sum = 0;

        for ($i = 3; $i <= $number; $i++) {
            if ($i % 3 === 0 || $i % 5 === 0) {
                $sum += $i;
            }
        }

        return $sum;
    }

    /**
     * @param string $message
     * @return string
     */
    private function cleanUpMessage(string $message): string
    {
        return preg_replace('/^\<.{0,}\>\ /', '', $message);
    }

    /**
     * @param string $message
     * @return int
     */
    private function getNumber(string $message): int
    {
        $match = [];

        preg_match('/[0-9]{1,}/', $message, $match);

        return count($match) ? (int)$match[0] : 0;
    }

    /**
     * @param $number
     * @return bool
     */
    private function validateNumber($number): bool
    {
        return $number > 0 && $number < 10000;
    }
}

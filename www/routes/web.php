<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Route $router*/

$router->get('/', function () use ($router) {
    return 'Cats are awesome 🐱';
});

$router->group(['middleware' => ['slack']], function () use ($router) {
    $router->post('/euler', 'HandleController@handle');
});

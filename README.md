# Slack bot to solve Euler problem #1

## Installation

Tip: You need to have installed docker and docker-compose on your environment and domain which connected to your host

### Clone repository

`git clone git@bitbucket.org:itomych/euler_slackbot.git`

### Go to directory of cloned repository

`cd /path/to/dir`

### Configure .env file

`cp ./www/.env.example ./www/.env`

### Build docker containers

`docker-compose build`

### Up builded containers

`docker-compose up`

### Install composer deps

`docker exec -it euler_php composer install`

### Next you should configure slack to use it 

[Slack App Tutorial](https://api.slack.com/start/overview)

Path to bot: [Your_domain_address]/euler